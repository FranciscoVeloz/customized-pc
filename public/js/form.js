$(function($){
    $("form").submit(function(event){
        
        event.preventDefault();

        $.ajax({
            url: "https://formspree.io/franciscoveloz245@gmail.com", 
            method: "POST",
            data:{
                name: $("#name").val(),
                email: $("#email").val(),
                message: $("#message").val()
            },

            dataType: "json"
        }).done(function(){
            $("#name").val("");
            $("#email").val("");
            $("#message").val("");
            alert("Mensaje enviado");
        }).fail(function(){
            alert("Mensaje no enviado");
        });
    });
});